/*
 * ProyectoFinal.c
 *
 * Created: 1/9/2019 14:57:17
 * Author : 534456
 */ 

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include "ADC.h"
#include "lcd.h"
#include "rtc.h"




int main(void)
{
		int k=0;
		int horactual;
		int minutoactual;
		int horaon=20;
		int minuton=31;
		int horatoff=20;
		int minutoff=32;
		
		dateTime fecha;

		
    	float Adc = 100;
    	float humedad = 0;
		
		int humedadt = 70;             //Variable humedad targget
		int  m = 1;      //Varibale contadora para los menus
		
    	char buff[16];

    	DDRB |= 0x10; //ld293
    	DDRC |= 0x10; //adc, botones,
    	PORTC|= 0x1E;
    	
				
		lcd_init(LCD_DISP_ON);
		
    	ConfigADC();
    	InitADC();
    	
    	lcd_home();
		
		sprintf(buff," ");
		
		Adc = ReadADC();
		humedad = (-98*(Adc-409)/615)+98;

    	while (1)
    	{
			fecha = get_date_time();
			
			horactual=fecha.hour;
			minutoactual=fecha.minute;
			
			if ((horactual==horaon)&(minutoactual==minuton))
			{
				PORTC &= 0xEF;
			}
			if ((horactual==horatoff)&(minutoactual==minutoff))
			{
				PORTC |= 0x10;
			}
			
	    	
	    	if (((int)fecha.minute%5==0)&(k==0))
	    	{
		    	Adc = ReadADC();
		    	humedad = (-98*(Adc-409)/615)+98;			

			}
		   	if ((int)fecha.minute%5!=0){
			   k=0;
			}
			
			if ( !(PINC & 0x08)){
				//while (!(PINC & 0x08));
				m ++;
				if (m == 5){
					
					m = 1;
				}
			}
			
			lcd_home();
			
			switch (m){
				case 1:
				sprintf(buff," ");
				lcd_clrscr();
				lcd_home();
				sprintf(buff,"HUMEDAD ACT: %02i ",(int)humedad); //
				lcd_puts(buff);
				lcd_gotoxy(0,1);
				sprintf(buff," ");
				sprintf(buff,"HORA ACT:  %02i:%02i", horactual, minutoactual);
				lcd_puts(buff);
				_delay_ms(500);
				break;
				
				case 2:
				if ( !(PINC & 0x02)){ //Sensar pull-up pin
					//while(!(PINC & 0x02));
					humedadt ++;
					if (humedadt == 99){
						humedadt = 1;
					}
				}
				if ( !(PINC & 0x04)){
					//while(!(PINC & 0x04));
					humedadt --;
					if (humedadt == -1){
						humedadt = 98;
					}
				}
				sprintf(buff," ");
				lcd_clrscr();
				lcd_home();
				sprintf(buff,"HUMEDAD ACT: %02i ",(int)humedad);
				lcd_puts(buff);
				lcd_gotoxy(0,1);
				sprintf(buff," ");
				sprintf(buff,"HUMEDAD SET: %02i ",humedadt);
				lcd_puts(buff);
				_delay_ms(500);
				
				break;
				
				case 3:
				if ( !(PINC & 0x02)){ //Sensar pull-up pin
					minuton++;
					if (minuton == 60){
						horaon ++;
						if (horaon==24)
						{
							horaon=0;
						}
						minuton = 0;
					}
					
				}
				if ( !(PINC & 0x04)){ //Sensar pull-up pin
					minuton--;
					if (minuton == -1){
						horaon --;
						if (horaon==-1)
						{
							horaon=23;
						}
						minuton = 59;
					}
				}
				lcd_clrscr();
				lcd_home();
				sprintf(buff," ");
				sprintf(buff,"HORA ACT: %02i:%02i ",horactual, minutoactual);
				lcd_puts(buff);
				lcd_gotoxy(0,1);
				sprintf(buff," ");
				sprintf(buff,"HORA ON:  %02i:%02i", horaon, minuton);
				lcd_puts(buff);
				lcd_gotoxy(10,1);
				sprintf(buff,"%02i",horaon);
				lcd_puts(buff);
				_delay_ms(500);
				
				break;
				
				case 4:
				if ( !(PINC & 0x02)){ //Sensar pull-up pin
					minutoff ++;
					if (minutoff == 60){
						horatoff ++;
						if (horatoff==24)
						{
							horatoff=0;
						}
						minutoff = 0;
					}
					
				}
				if ( !(PINC & 0x04)){ //Sensar pull-up pin
					minutoff --;
					if (minutoff == -1){
						horatoff --;
						if (horatoff==-1)
						{
							horatoff=23;
						}
						minutoff = 59;
					}
				}
				lcd_clrscr();
				lcd_home();
				sprintf(buff," ");
				sprintf(buff,"HORA ACT: %02i:%02i ",horactual, minutoactual);
				lcd_puts(buff);
				lcd_gotoxy(0,1);
				sprintf(buff," ");
				sprintf(buff,"HORA OFF: %02i:%02i ",horatoff, minutoff);
				lcd_puts(buff);
				lcd_gotoxy(10,1);
				sprintf(buff,"%02i",horatoff);
				lcd_puts(buff);
				_delay_ms(500);
				//while(!(PINC & 0x02)|!(PINC & 0x04));
				break;
				
			}
				
			if ((humedad < humedadt)&((int)fecha.minute%5==0)&(k==0))
			{
				PORTB |= 0x10;
				_delay_ms(10000);
				PORTB &= 0xEF;
				k=1;
			}
			else{
				PORTB &= 0xEF;
			}
					
			

    	}
    	
    	
    	return 1;
}

