/*
 * ADC.c
 *
 * Created: 5/08/2019 3:23:29 p. m.
 * Author : PC-X
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

void ConfigADC(){
	ADMUX |= (1<<REFS0);
	//ADMUX &= 0xF0;
	ADCSRA |= (1<<ADEN)|(1<<ADPS1)|(1<<ADPS0);
}

void InitADC(){
	ADCSRA |= (1<<ADSC);
}

int ReadADC(){
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));
	return ADC;
}